package br.com.uboard.config.consumer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import br.com.uboard.model.CredentialsDTO;
import br.com.uboard.service.CredentialsService;

@Component
public class SyncCredentialsConsumer {

	private static final Logger LOGGER = LoggerFactory.getLogger(SyncCredentialsConsumer.class);
	private static final String SYNC_CREDENTIALS_TOPIC = "sync-credentials-topic";

	private CredentialsService credentialsService;

	public SyncCredentialsConsumer(CredentialsService credentialsService) {
		this.credentialsService = credentialsService;
	}

	@KafkaListener(groupId = "sync-credentials-group", topics = SYNC_CREDENTIALS_TOPIC, containerFactory = "credentialsContainerFactory")
	public void receive(@Payload List<CredentialsDTO> credentialsList) throws Exception {
		try {
			LOGGER.info("Getting {} credentials to sync", credentialsList.size());
			this.credentialsService.sync(credentialsList);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

}
