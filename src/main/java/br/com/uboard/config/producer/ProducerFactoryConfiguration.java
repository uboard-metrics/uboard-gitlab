package br.com.uboard.config.producer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import br.com.uboard.model.UserMilestonesDTO;

@Configuration
public class ProducerFactoryConfiguration {

	@Autowired
	private KafkaProperties properties;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Bean
	public ProducerFactory<?, ?> producerFactory() {
		HashMap<String, Object> configs = new HashMap<String, Object>();
		configs.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, properties.getBootstrapServers());
		configs.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		configs.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
		return new DefaultKafkaProducerFactory(configs, new StringSerializer(), new JsonSerializer());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Bean
	public KafkaTemplate<String, Serializable> jsonKafkaTemplate(ProducerFactory producerFactory) {
		return new KafkaTemplate<>(producerFactory);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Bean
	public KafkaTemplate<String, List<UserMilestonesDTO>> sendStatisticsKafkaTemplate(ProducerFactory producerFactory) {
		return new KafkaTemplate<>(producerFactory);
	}
}
