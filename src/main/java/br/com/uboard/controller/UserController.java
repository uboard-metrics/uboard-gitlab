package br.com.uboard.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.uboard.model.CredentialsDTO;
import br.com.uboard.model.UserDTO;
import br.com.uboard.service.GitlabService;

@RestController
@RequestMapping("/user")
public class UserController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

	private GitlabService gitlabService;

	public UserController(GitlabService gitlabService) {
		this.gitlabService = gitlabService;
	}

	@PostMapping
	public ResponseEntity<UserDTO> fetchUser(@RequestBody @Valid CredentialsDTO credentialsDTO) throws Exception {
		try {
			UserDTO response = this.gitlabService.fetchUserByCredentials(credentialsDTO);
			return ResponseEntity.ok(response);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}
}
