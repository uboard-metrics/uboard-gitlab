package br.com.uboard.controller.scheduler;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.uboard.config.producer.Topics;
import br.com.uboard.model.MilestoneDTO;
import br.com.uboard.model.UserMilestonesDTO;
import br.com.uboard.service.CredentialsService;
import br.com.uboard.service.GitlabService;

@Component
@EnableAsync
public class SendStatisticsScheduler {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendStatisticsScheduler.class);

	private KafkaTemplate<String, List<UserMilestonesDTO>> kafkaTemplate;

	private GitlabService gitlabService;

	private CredentialsService credentialsService;

	public SendStatisticsScheduler(KafkaTemplate<String, List<UserMilestonesDTO>> kafkaTemplate,
			GitlabService gitlabService, CredentialsService credentialsService) {
		this.kafkaTemplate = kafkaTemplate;
		this.gitlabService = gitlabService;
		this.credentialsService = credentialsService;
	}

	@Async
	@Scheduled(fixedDelay = 180000)
	public void sendStatistics() {
		LOGGER.info("Fetching and sending milestones statistics...");

		Set<String> credentialsKeys = this.credentialsService.getCredentialsKeys();
		List<UserMilestonesDTO> userMilestones = new ArrayList<>();

		if (credentialsKeys != null && !credentialsKeys.isEmpty()) {
			credentialsKeys.forEach(userUUID -> {
				try {
					Set<MilestoneDTO> milestonesByUser = this.gitlabService.fetchMilestonesByUser(userUUID);
					UserMilestonesDTO userMilestonesDTO = new UserMilestonesDTO();
					userMilestonesDTO.setUserUUID(userUUID);
					userMilestonesDTO.setMilestones(milestonesByUser);
					userMilestones.add(userMilestonesDTO);
				} catch (Exception e) {
					LOGGER.error(e.getMessage(), e);
				}
			});

			if (!userMilestones.isEmpty()) {
				this.kafkaTemplate.send(Topics.SEND_STATISTICS_TOPIC, userMilestones);
			}
		}

	}
}
