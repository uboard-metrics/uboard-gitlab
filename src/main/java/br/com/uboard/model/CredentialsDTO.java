package br.com.uboard.model;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

public class CredentialsDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotBlank(message = "Gitlab address must be provided")
	private String address;

	@NotBlank(message = "Gitlab user token must be provided")
	private String token;

	private String userUUID;

	private Boolean removable;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUserUUID() {
		return userUUID;
	}

	public void setUserUUID(String userUUID) {
		this.userUUID = userUUID;
	}

	public Boolean getRemovable() {
		return removable;
	}

	public void setRemovable(Boolean removable) {
		this.removable = removable;
	}

}
