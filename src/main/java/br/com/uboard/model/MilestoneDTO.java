package br.com.uboard.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.gitlab4j.api.models.Milestone;

public class MilestoneDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String title;
	private String state;
	private Date startDate;
	private Date dueDate;
	private ScopeDTO scope;
	private List<MemberDTO> members = new ArrayList<>();
	private List<IssueDTO> issues = new ArrayList<>();

	public MilestoneDTO() {

	}

	public MilestoneDTO(Milestone milestone) {
		this.id = milestone.getId();
		this.title = milestone.getTitle();
		this.state = milestone.getState();
		this.startDate = milestone.getStartDate();
		this.dueDate = milestone.getDueDate() != null ? milestone.getDueDate() : null;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public ScopeDTO getScope() {
		return scope;
	}

	public void setScope(ScopeDTO scope) {
		this.scope = scope;
	}

	public List<MemberDTO> getMembers() {
		return members;
	}

	public void setMembers(List<MemberDTO> members) {
		this.members = members;
	}

	public List<IssueDTO> getIssues() {
		return issues;
	}

	public void setIssues(List<IssueDTO> issues) {
		this.issues = issues;
	}
}
