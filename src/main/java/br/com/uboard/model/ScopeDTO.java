package br.com.uboard.model;

import java.io.Serializable;

import org.gitlab4j.api.models.Group;
import org.gitlab4j.api.models.Project;

import br.com.uboard.model.enums.ScopeTypeEnum;

public class ScopeDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private String name;
	private String type;

	public ScopeDTO() {

	}

	public ScopeDTO(Project project) {
		this.setId(project.getId());
		this.setName(project.getName());
		this.setType(ScopeTypeEnum.PROJECT.getName());
	}

	public ScopeDTO(Group group) {
		this.setId(group.getId());
		this.setName(group.getName());
		this.setType(ScopeTypeEnum.GROUP.getName());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
