package br.com.uboard.model.enums;

public enum ScopeTypeEnum {

	GROUP("GROUP"), PROJECT("PROJECT");

	private String name;

	private ScopeTypeEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public static ScopeTypeEnum getByName(String name) {
		for (ScopeTypeEnum scope : ScopeTypeEnum.values()) {
			if (scope.getName().equals(name)) {
				return scope;
			}
		}

		throw new IllegalArgumentException("No matching type for value: " + name);
	}
}
