package br.com.uboard.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.uboard.model.CredentialsDTO;

@Service
public class CredentialsService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CredentialsService.class);

	private Map<String, CredentialsDTO> credentials = new HashMap<String, CredentialsDTO>();

	public void sync(List<CredentialsDTO> credentialsList) {
		if (!credentialsList.isEmpty() && credentialsList != null) {
			credentialsList.forEach(credentialsDTO -> {
				if (!credentialsDTO.getRemovable()) {
					this.add(credentialsDTO);
				} else {
					this.remove(credentialsDTO.getUserUUID());
				}
			});
		}

		LOGGER.debug("Has {} credentials", this.credentials.size());
	}

	public void add(CredentialsDTO credentialsDTO) {
		LOGGER.debug("Adding/updating user credentials with UUID: {}", credentialsDTO.getUserUUID());
		credentials.put(credentialsDTO.getUserUUID(), credentialsDTO);
	}

	public void remove(String userUUID) {
		LOGGER.debug("Removing user credentials with UUID: {}", userUUID);
		credentials.remove(userUUID);
	}

	public CredentialsDTO get(String userUUID) {
		return credentials.getOrDefault(userUUID, null);
	}

	public Set<String> getCredentialsKeys() {
		return credentials.keySet();
	}

}
