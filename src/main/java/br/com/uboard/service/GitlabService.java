package br.com.uboard.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.gitlab4j.api.Constants.IssueScope;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.models.Group;
import org.gitlab4j.api.models.GroupFilter;
import org.gitlab4j.api.models.Issue;
import org.gitlab4j.api.models.IssueFilter;
import org.gitlab4j.api.models.Member;
import org.gitlab4j.api.models.Milestone;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.com.uboard.model.CredentialsDTO;
import br.com.uboard.model.IssueDTO;
import br.com.uboard.model.MemberDTO;
import br.com.uboard.model.MilestoneDTO;
import br.com.uboard.model.ScopeDTO;
import br.com.uboard.model.UserDTO;
import br.com.uboard.model.enums.ScopeTypeEnum;

@Service
public class GitlabService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GitlabService.class);

	private CredentialsService credentialsService;

	public GitlabService(CredentialsService credentialsService) {
		this.credentialsService = credentialsService;
	}

	private GitLabApi openConnection(CredentialsDTO credentialsDTO) throws Exception {
		try {
			return new GitLabApi(credentialsDTO.getAddress(), credentialsDTO.getToken());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new Exception(e.getMessage());
		}
	}

	public UserDTO fetchUserByCredentials(CredentialsDTO credentialsDTO) throws Exception {
		try {
			GitLabApi client = this.openConnection(credentialsDTO);

			if (client == null || client.getUserApi() == null) {
				throw new Exception("Unable to connect to Gitlab as the data provided is invalid");
			}

			User currentUser = client.getUserApi().getCurrentUser();
			if (currentUser == null) {
				throw new Exception(
						"User was not found in Gitlab. Make sure the credentials are correct and try again");
			}

			UserDTO userDTO = new UserDTO(currentUser);
			userDTO.setAddress(credentialsDTO.getAddress());
			userDTO.setToken(credentialsDTO.getToken());

			return userDTO;
		} catch (GitLabApiException e) {
			LOGGER.error(e.getMessage(), e);
			throw new GitLabApiException(e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new Exception(e.getMessage());
		}
	}

	public Set<MilestoneDTO> fetchMilestonesByUser(String userUUID) throws Exception {
		CredentialsDTO credentialsDTO = this.credentialsService.get(userUUID);
		if (credentialsDTO == null) {
			return new HashSet<>();
		}

		Set<MilestoneDTO> milestones = new HashSet<>();

		GitLabApi client = this.openConnection(credentialsDTO);

		List<ScopeDTO> scopes = new ArrayList<>();
		scopes.addAll(this.fetchGroups(client));
		scopes.addAll(this.fetchProjetcs(client));

		for (ScopeDTO scopeDTO : scopes) {
			if (scopeDTO.getType().equals(ScopeTypeEnum.GROUP.getName())) {

				List<Milestone> groupMilestones = client.getMilestonesApi().getGroupMilestones(scopeDTO.getId());
				if (groupMilestones != null && !groupMilestones.isEmpty()) {
					groupMilestones.forEach(groupMilestone -> {
						ScopeTypeEnum scope = ScopeTypeEnum.GROUP;
						List<MemberDTO> members = this.fetchMembers(client, scopeDTO.getId(), scope);

						MilestoneDTO milestoneDTO = new MilestoneDTO(groupMilestone);
						milestoneDTO.setScope(scopeDTO);
						milestoneDTO.setMembers(members);
						milestones.add(milestoneDTO);
					});
				}
			} else {

				List<Milestone> projectMilestones = client.getMilestonesApi().getMilestones(scopeDTO.getId());
				if (projectMilestones != null && !projectMilestones.isEmpty()) {
					projectMilestones.forEach(projectMilestone -> {
						ScopeTypeEnum scope = ScopeTypeEnum.PROJECT;
						List<MemberDTO> members = this.fetchMembers(client, scopeDTO.getId(), scope);

						MilestoneDTO milestoneDTO = new MilestoneDTO(projectMilestone);
						milestoneDTO.setScope(scopeDTO);
						milestoneDTO.setMembers(members);
						milestones.add(milestoneDTO);
					});
				}
			}

		}

		if (milestones == null || milestones.isEmpty()) {
			return new HashSet<>();
		}

		milestones.forEach(milestone -> {
			ScopeTypeEnum scopeType = ScopeTypeEnum.getByName(milestone.getScope().getType());
			List<IssueDTO> issues = this.fetchIssues(client, milestone, scopeType);
			milestone.setIssues(issues);
		});

		return milestones;
	}

	public List<IssueDTO> fetchIssues(GitLabApi client, MilestoneDTO milestonDTO, ScopeTypeEnum scope) {
		try {
			List<Issue> issues = new ArrayList<>();
			if (scope == ScopeTypeEnum.PROJECT) {
				IssueFilter issueFilter = new IssueFilter().withScope(IssueScope.ALL)
						.withMilestone(milestonDTO.getTitle());
				issues = client.getIssuesApi().getIssues(milestonDTO.getScope().getId(), issueFilter);
				if (issues == null || issues.isEmpty()) {
					return new ArrayList<>();
				}
			} else {
				IssueFilter issueFilter = new IssueFilter().withScope(IssueScope.ALL)
						.withMilestone(milestonDTO.getTitle());
				issues = client.getIssuesApi().getGroupIssues(milestonDTO.getScope().getId(), issueFilter);
				if (issues == null || issues.isEmpty()) {
					return new ArrayList<>();
				}
			}

			return issues.stream().map(IssueDTO::new).collect(Collectors.toList());
		} catch (GitLabApiException e) {
			LOGGER.error(e.getMessage(), e);
			return new ArrayList<>();
		}
	}

	public List<MemberDTO> fetchMembers(GitLabApi client, Long id, ScopeTypeEnum scope) {
		try {
			if (scope == ScopeTypeEnum.GROUP) {
				List<Member> members = client.getGroupApi().getAllMembers(id);
				if (members == null || members.isEmpty()) {
					return new ArrayList<>();
				}

				return members.stream().map(MemberDTO::new).collect(Collectors.toList());
			} else {
				List<Member> members = client.getProjectApi().getAllMembers(id);
				if (members == null || members.isEmpty()) {
					return new ArrayList<>();
				}

				return members.stream().map(MemberDTO::new).collect(Collectors.toList());
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return new ArrayList<>();
		}
	}

	public List<ScopeDTO> fetchProjetcs(GitLabApi client) {
		try {
			List<Project> projects = client.getProjectApi().getMemberProjects();
			if (projects == null || projects.isEmpty()) {
				return new ArrayList<>();
			}

			return projects.stream().map(ScopeDTO::new).collect(Collectors.toList());
		} catch (GitLabApiException e) {
			LOGGER.error(e.getMessage(), e);
			return new ArrayList<>();
		}
	}

	public List<ScopeDTO> fetchGroups(GitLabApi client) {
		try {
			List<Group> groups = client.getGroupApi().getGroups(new GroupFilter().withOwned(true));
			if (groups == null || groups.isEmpty()) {
				return new ArrayList<>();
			}

			return groups.stream().map(ScopeDTO::new).collect(Collectors.toList());
		} catch (GitLabApiException e) {
			LOGGER.error(e.getMessage(), e);
			return new ArrayList<>();
		}
	}
}
